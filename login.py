import pyautogui
from time import sleep
import random
import cv2
from pyscreeze import ImageNotFoundException

pyautogui.PAUSE = 3
pyautogui.FAILSAFE = True

# Sleep so we have time to alt tab to the correct window
sleep(2)

RANDO_QUEUE = random.randrange(5, 45, 1)


def main():
    # click_enter_world()
    decision = input('Would you like to enter world, afk, or are you in queue? Type enter, queue, or afk: ')

    # figure out where in the process we are

    if decision == 'afk':
        afk()
    elif decision == 'queue':
        work_queue()
    else:
        click_enter_world()


def work_queue():
    print('working the queue. sleeping for a few minutes then checking again')
    sleep(180)
    
    while True:
        try:
            enter = pyautogui.locateCenterOnScreen(r'img/enter.png', confidence=0.8)
            print('looks like we made it to character select')
            sleep(5)
            pyautogui.press('enter')
            break
        except:
            print('looks like the queue is still going strong')
            pass
    
    afk()

def click_enter_world():
    sleep(5)
    print('Looking for a good button to click.')

    while True:
        try:
            enter = pyautogui.locateCenterOnScreen(r'img/enter.png', confidence=0.8)
            # toon = pyautogui.locateCenterOnScreen(r'img/character.png', confidence=0.8)
            print('Servers are up! Picking toon then clicking enter world!')
            # pyautogui.click(toon, clicks=1, button='left')
            sleep(5)
            pyautogui.click(enter, clicks=1, button='left')
            break
        except:
            print("Server's aren't up yet, let's try it again")
            pass

    afk()


def afk():
    disconnected = False
    
    try:
        while True:
            disconnected = pyautogui.locateCenterOnScreen(r'img/discon.png', confidence=0.8)
            if disconnected:
                print('looks like youve been disconnected. trying to reconnect')
                click_reconnect()
            else:
                sleeping = get_rando()
                print(f'Keeping you in-game. Moving again in {sleeping} seconds.')
                sleep(sleeping)

                pyautogui.press(['space', 'a', 'd', 'space'])
    except KeyboardInterrupt:
        print('\nExiting')


def click_reconnect():
    try:
        print("reconnecting, then entering world")
        pyautogui.press('enter')
        sleep(10)
        pyautogui.press('enter')
        sleep(30)
        pyautogui.press('enter')
        sleep(45)
        afk()
    except:
        print("thought we were at the reconnect screen, but we arent. maybe we're at character select")
        sleep(30)
        click_enter_world()



def get_rando():
    slept = random.randrange(10, 240, 1)

    return slept


if __name__ == '__main__':
    print('Starting her up. Good luck.')
    main()
